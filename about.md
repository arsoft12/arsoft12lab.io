---
bg: "owl.jpg"
layout: page
title: "About"
crawlertitle: "Why and how this blog was created"
permalink: /about/
summary: "About this blog"
active: about
---

You’ll find this pages, relative information to SOFTWARE ARCHITECTURE asing.

This site is powered by:
  - Gitlab
  - jekyll
  - [Voyager](http://www.unix-lab.org/voyager/) jekyll theme
  - reveal.js

Thanks!
