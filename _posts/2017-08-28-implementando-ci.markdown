---
bg: "tools.jpg"
layout: post
title:  "Implementando Laboratorio Integracion Continua con GitLab."
date:   2017-08-13 11:32:14 -0300
crawlertitle: "General Description"
summary: "High Level Deign"
categories: jekyll hwarch posts
tags: ['front-end']
excerpt_separator: <!--more-->
---
Implementando un Laboratorio para CI en GitLab

## Resumen de actividades para la session 1:
- Clonar repositorio basico.
- Realizar seguimiento de cada una de las fases que permite GitLab.
- Identificar el codigo al que se quiere realizar el TEST.
- Identificar el codigo con el que se realiza el test.
- Identificar los pasos que son ejecutados luego del PUSH sobre el repositorio.

<!--more-->

<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/software-architecture-docs/raw/master/Setup_Lab_Php_GitLabCI.md %}
