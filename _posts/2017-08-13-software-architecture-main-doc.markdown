---
bg: "tools.jpg"
layout: post
title:  "General document Software Architecture."
date:   2017-08-13 11:32:14 -0300
crawlertitle: "General Description"
summary: "High Level Deign"
categories: jekyll hwarch posts
tags: ['front-end']
excerpt_separator: <!--more-->
---
Software architecture refers to the high level structures of a software system, the discipline of creating such structures, and the documentation of these structures.

### Previus Research:
- [Software engineering Art State](http://www.pepitosoft.com/jekyll/hwarch/posts/ingenieria-software-finalwork/)
  - [Presentation format ](http://www.pepitosoft.com/presentation/ingeneria-software-slide/).
- [Employing git on this project](http://www.pepitosoft.com/posts/hwarch/employing_git_on_this_project/)

<!--more-->

<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/software-architecture-docs/raw/master/Readme.md %}
