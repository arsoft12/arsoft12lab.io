---
bg: "tools.jpg"
layout: post
title:  "General document Software engineering."
date:   2016-03-24 15:32:14 -0300
crawlertitle: "General Description"
summary: "High Level Deign"
categories: jekyll hwarch posts
tags: ['front-end']
excerpt_separator: <!--more-->
---
Software engineering is the application of engineering to the design, development, implementation, testing and maintenance of software in a systematic method.

<!--more-->

<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/oemunoz/software_engineering/raw/master/2016-09-17-rad-software-md.markdown %}
