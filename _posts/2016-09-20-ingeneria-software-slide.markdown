---
layout: slide
title: Presentations&#58; Presentation Ing. Software.
date:   2016-03-24 15:32:14 -0300
description: Article on markdown format.
categories: presentation
theme: black
transition: slide
excerpt_separator: <!--more-->
---
<p style='color:#214063'>
Presentacion final Ing. Software.
</p>
<!--more-->


<section data-background="https://gitlab.com/oemunoz/software_engineering/raw/master/images/background.png" data-markdown data-separator="^----" data-separator-vertical="^====" >
<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/oemunoz/software_engineering/raw/master/2016-09-17-rad-software-md.markdown %}
</section>
